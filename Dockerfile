FROM python:3.8-slim-buster
ENV PYTHONUNBUFFERED 1

# Install cron
RUN apt-get update && apt-get install -y cron build-essential

# Make the code directory
RUN mkdir /code
RUN mkdir /output
RUN mkdir /geodata
WORKDIR /code

# Install requirements for the covid charts script
COPY requirements.txt /code/
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

# Add files
ADD run.sh /run.sh
ADD entrypoint.sh /entrypoint.sh
 
RUN chmod +x /run.sh /entrypoint.sh

ENTRYPOINT /entrypoint.sh

# Run a command to ensure the container does not exit
#CMD ["sleep", "infinity"]
