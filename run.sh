#!/bin/bash
cd /code
python Graphs.py

cp /code/temp/index.html /output/covid_stats.html
cp /code/pickles/* /pickles/
cp /code/geodata/* /geodata/
