#!/bin/zsh
docker run -d \
           -v /Users/steve/Documents/Coding/Python/BokehCharts/python-charts/:/code/ \
           -v /Users/steve/Documents/Coding/Python/website/mainsite/static/covidstats/:/output/ \
           -v /Users/steve/Documents/Coding/Python/website/mainsite/charts/BokehPlots/pickles/:/pickles/ \
           -v /Users/steve/Documents/Coding/Python/website/mainsite/charts/BokehPlots/geodata/:/geodata/ \
           bokeh_image
